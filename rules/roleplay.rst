##############
Roleplay Rules
##############
.. note::

  These are our core rules which are applied universally while roleplaying.

.. _UA: https://forums.owlgaming.net/forms/10-upper-administration-contact-ooc/


Always Roleplay
===============
All players must remain in character at all times unless allowed to go out of character by an administrator or they are themselves an administrator.

*"Roleplay now,* `report <https://owlgaming.net/support/>`_ *later."*

Brawls
------
Standard procedure is to fight in an RP manner, however, players can OOCly agree to perform a fight in a brawl done with GTA physics.

Vehicular Pursuits
------------------
An exception of always roleplay is doing a PIT maneuver during a chase.

MetaGaming
==========
MetaGaming is when someone uses out of character (OOC) information for in character (IC) purposes. If you attempt to
incite MetaGaming, then you're also breaking rules.

PowerGaming
===========
A player can be described as a PowerGamer if he or she presumes or declares that his or her own action against another player
character is successful without giving the other player character the freedom to act on his or her own decisions. This includes
but is not limited to:

* Forcing actions upon a player.
* Failing to allow a player to roleplay their own actions.
* Use of items which you haven't physically obtained.
* Acting superhuman.

Character Development
-----------------------
Character development plays a vital role in what is considered PowerGaming. A character that goes to the gym and works out regularly
may be in better physical condition than others involved in the situation.

Special Characters
^^^^^^^^^^^^^^^^^^
Special Characters are characters which have a particular subset of skills (superior strength, shooting, stamina, etc), such as but not
limited to:

* Mentally Challenged
* Skilled Martial Artists
* Members of Special Operations Forces

These characters must have `UA`_ approval prior to roleplaying as such.

Vehicles
----------
Vehicles which are used as they are not designed, such a lowrider offroading is considered PowerGaming as the vehicle would not be
able to sustain such conditions without breaking or becoming inoperable.

Deathmatching
=============
Deathmatching is the act of killing another persons character without sufficient reason or proper roleplay.

Death
============

Player Kills
------------
A player kill is when your character is killed, simulating unconsciousness and amnesia which extends as far back as that particular roleplay situation's beginning.

**Example**

  John Smith goes to a bar in a bad part of town and meets a particularly violent drunk named Wilson LaRoche who while minding his own     business, hits his girlfriend a few times. John Smith, being a white knight of the situation, tries to intervene. The two get into a     physical altercation and while Wilson is inebriated he loses some self control, kicking John Smith repeatedly in the face after he       collapsed against an arcade machine. Slumped in the corner, John Smith is player killed where he his health is depleted and he is       killed script-wise.

No application is needed to player kill someone. Only a solid in character reason.

Roleplaying After a Player Kill
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If a character is player killed and the scene is left after they respawn, the players involved should roleplay that the unconscious character is found by a good Samaritan, emergency services were called and they were taken to the hospital. The person that was player killed would then after they respawn at the hospital, roleplay in the hospital for a period of time for their injuries to heal and continue to roleplay those injuries accordingly.

Do not respawn and immediately run around as if nothing happened!

Character Kills
---------------
A character kill is when your character is permanently killed and the ability to access that character is disabled via script function. They serve as a means of permanently ending a character's life and their story. Character kills should not be taken lightly. Remember, a majority of situations can be dealt with before resorting to a character kill by beating, player kill, etc.

**Example**

  John Smith over time due to his addictive personality accumulated gambling debt from a local underground poker establishment. Weeks pass     and John Smith fails to make consistent payments on the gambling debt, totaling $50,000. He is threatened and beat up but continues     failing to pay his debt. The poker facility manager and crime boss decides to kill John Smith as retribution for not paying back the     money and to send a message to the other people that owe him money that they should pay in a timely manner.

  A sum of $50,000 is a substantial amount of money. If a smaller amount is owed, say, $10,000, a severe beating may be in order and perhaps the   crime boss' goons break into his house and steal stuff from him to get some payment and a loan would never be given to John Smith       again because $10,000 would likely not warrant something as severe as murder.

An `application <https://forums.owlgaming.net/forms/20-general-administration-character-kill-appeal-ooc/>`_ is necessary to character kill someone due to the severity of it. If your character kill application is sensitive, you may send make a `private <https://forums.owlgaming.net/forms/8-senior-administration-private-character-kill-application-ooc/>`_ request which is sent to our senior administrators. Private character kills may be sent to upper administration members as well for the most privacy. Additionally, character kills may be accepted in game by a single administrator for situations where you require a quick response where you otherwise could not wait for an application.

In order for a character kill to be valid, the character being killed should generally be killed script wise. There are a few types of exceptions to this. If for example someone's foot is exposed and it is shot over and over and they die script wise, they would not necessarily realistically die. Thus, if they received reasonable medical treatment before they bled out, they'd survive and lose their foot. On the other hand, if that person hadn't received prompt medical attention, they would bleed out and die, warranting a character kill even if they hadn't died scriptwise. Another example of this would be if someone is run over by a vehicle. With the game physics, they may not lose very much health, but in reality, they would very easily be killed at a high speed impact.

Character Kill Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^
A character kill scenario is exactly as it sounds, a scenario of serious roleplay where character kills are issued to characters that die. These are common in roleplay events and large or serious situations.

In order for a character kill scenario to be valid, there must be proof. An administrator must witness the situation or there must be roleplay logs, a video, credible eye witnesses or comprehensive screen shots then shown to the handling administrator. Character kill scenarios may happen at any time an administrator deems necessary, thus, players should always be realistic and careful as they would in real life.

A character kill scenario may not necessarily be publicly mentioned to people involved to prevent metagaming where people shoot just to get a character kill where they otherwise wouldn't have shot if they didn't know them shooting someone was a guaranteed character kill on another player.

Below are some brief examples of character kill scenarios. They are by no means a comprehensive list showing the only situations you may be character killed but instead serve to give you a better idea of what are the most common scenarios.

Disregard for Life
""""""""""""""""""
Someone showing disregard for their own life or unnecessary risk such as acting suicidally or     generally not caring for their actions as carefully as they would in real life. Some more detailed examples of this would be:

  * Pulling a gun out on a police officer during a traffic stop when you are going to receive a traffic infraction, your character is then killed.

  * Reckless handling of a vehicle or aircraft and crashing it which would clearly result in death, especially at a very high rate of speed.

  * Police acting like they’re invincible, failing to follow proper safety protocol, acting out unrealistically in situations where they normally wouldn’t.
  
Please note, not all forms of disregard must or even should result in a character kill. The circumstances of the scenario and context matter greatly. If someone is resisting capture or kidnapping against 4 people, their chances of actually escaping and telling the police is slim to none which means if one of those 4 kidnappers is a hothead and shoots the person resisting capture, it should not be immediately deemed a character kill. The weight of their actions should be taken into account.

Organized Robberies
"""""""""""""""""""
Robberies which are organized, whether spur of the moment or heavily pre-planned are common circumstances in which a player (the robber) may be character killed. This includes heists, ammunation robberies, quick 24/7 robberies, etc.

  * Your character is being robbed by someone who is wielding a gun, rather than give up the $100 you have in your wallet, you pull your own gun out and attempt to shoot the robber. The robber then shoots and kills you.

  * A car of gang members burst into a convenient store to rob it. Inside is someone carrying a gun. Upon seeing the men aim a gun at the cashier, they draw their gun and manage to shoot two of the robbers and kill them.

Furthermore, just like with disregard for life, not all robbery related deaths have to be character kills. If robbers jump out of a van and aim guns at a woman on the side of the street, it is a normal, instinctive reaction for someone to immediately run at the first sight of a gun. If that woman is then shot in the back, her death wouldn't be considered a character kill. If she is, however, cornered in an allyway for a moment with guns to her face and she tries to run or escape or physically resist, she's had some time to think about the scenario and running isn't a purely instinctual response at that point, thus, her death could be a character kill. (Though again, it could just as easily be a player kill too! Those do have severe consequences!)

Planned or Supervised Events
""""""""""""""""""""""""""""
Some events such as large fires, catastrophic weather, terrorist attacks, freak accidents, etc, are pre-planned or setup and supervised by admins. Some naturally occurring events become these types of large situations such as a large gang shootout, large pile-up car accident, and so on. During these types of events players may be eligible for a character kill as well.

  * A plane crashes into a building where the fuel catches on fire and you are caught in the blaze.

  * A landslide beside a mountain which crushes you as a civil servant or simply there as a civilian.

  * A man hunt for a criminal within a certain area who is knowingly armed and dangerous.

Roleplaying Death
"""""""""""""""""
When any player roleplays death it may result in a character kill. A player cannot kill themselves or roleplay death and then have it appealed to come back, or try to roleplay coming back to life (unless assisted by CPR or defibrillators) within reasonable limits.

Infiltrating Organizations
""""""""""""""""""""""""""
Characters that infiltrate an organization to gain intelligence, insiders knowledge, evidence, or any other means of information that could harm the survival of an organization. This clause focuses primarily on law enforcement infiltrating criminal organizations, but is applicable the other way around or with criminal organizations infiltrating each other as well.

  * A detective joining a street gang to help police take them down. One of the gang members finds out and murders the detective.

  * A lawyer joining a law firm to plot and overthrow the organization, steal accounts, clients, etc. One of the scumbag lawyers involved hires a bum to stab the infiltrator in a spur of the moment idea just after a big argument.

  * A criminal or informant joining the police department to gain knowledge for a gang. A detective with corruption that finds out they have been deceived in a blind rage one night kills the informant.

Police Situations
"""""""""""""""""
A brief forewarning, not all shootings or scenarios need to be character kills. Just because a character kill may happen does not mmean it has to happen. Admins are encouraged to treat more shootings and situations as player kills and be more strict about character kills because it will generate more roleplay for everyone involved. 

If a situation arises where the death of a police officer is justified by the hands of (a) criminal(s) due to serious ramifications that the player would suffer if caught, a character kill may be enforced for both parties. Police officers as a whole though are not character killed if they are simply doing their job and character kills on police officers are also not justified if you are utilizing violence on a police officer to prevent yourself from being caught for less substantial crimes such as robbery, battery, drug dealing, weapon possession, etc. Players who escalate the level of force used against police officers cannot use that escalation to justify a character kill. An example of this will be below.

  * A criminal is fleeing police custody for a drug charge. If they shoot at police to escape this, it wouldn't be a character kill because the punishment they are fleeing from is not essentially life threatening like life in prison would be for murder. If the criminal fleeing police for a drug charge shoots at the police, even though this was escalated to potentially attempted murder, a character kill wouldn't be valid because the escalation was done by the fleeing party. Forcing a character kill on the officer at this point would mean that the vast majority of police deaths like this are character kills which is unsustainable.

  * A criminal has an arrest warrant out on them for murder which would result in life in prison. The player shoots at the police officer to prevent the officer from identifying and arresting them so they may escape. The police officer may be killed in this situation where substantial stakes are at risk. Foreknowledge is a mitigating factor in this circumstance. If the officer did not know there was an arrest warrant for murder, the chances of them being character killed are reduced. If they did know they had an arrest warrant on them for murder, the chances are increased.

  * During a gun deal where a high-level gang leader is involved a police officer pulls up on them. In order to protect the identity of the high-level gang leader and prevent an investigation which could cripple the gang, everyone flees and a few of the gang members at the deal shoot at the police officer, killing them.

Character Kill Clauses
^^^^^^^^^^^^^^^^^^^^^^
Factions may have a character kill clause that you inherently take upon yourself by associating with them. These factions are generally illegal ones. In order for a character kill clause to be valid it must be present on their thread and submitted to the Faction Team so they are aware of it and can validate your claim of the clause in the future to ensure it is not being made up.

Generally character kill clauses for factions cover anyone who is an associate and above. The criteria for someone to be character killed is nearly endless and is generally approved by a leader of the faction. This is the inherent risk in being part of illegal roleplay. You are considered an “associate” and above if you willingly take part in illegal activity with an associate or member of the faction.

Extreme or Disgusting Roleplay
===============================

Consent
-------
Every party involved, including witnesses, must OOCly agree to participate in any of the situations listed below:

* Rape
* Cannibalism
* Bestiality
* Necrophilia
* Sexual Harassment

You may withdraw your consent at anytime during the roleplay.

Prohibited
----------
Roleplay in the following list is prohibited in any circumstance:

* Sexual roleplay of minors (younger than 16)

Roleplay Binds
===============
Binds to draw or holster one handed weapons are allowed as they naturally have a faster draw time. Two handed weapons such as assault rifles, rifles, shotguns, etc. require a manually typed out /me to draw the weapon, unless it is easily accessible due to predetermined RP (gun racks, gun slings, gun on lap, etc).

Logging to Avoid
================
Players are forbidden from logging out during a roleplay unless approved by an administrator. Do  not join in a large roleplay situation if you cannot commit the time.

Law Enforcement Situations
--------------------------
After criminal activity in which Law Enforcement may become involved, you must wait 30 minutes prior to logging off.

Provoking
==========
Seeking attention from law enforcement or emergency services by shouting at them, making 911 calls to be chased, etc, is prohibited.

Evidence
=========
All actions may leave traces left behind from the roleplay. Such as, but not limited to:

* CCTV Footage
* Finger Prints
* Tire Treads or Shoe Imprints
* Broken Locks / Doors
* Glass Fragments
* Civilian Witnesses
* Etcetera

Notes should be dropped indicating this evidence and information must be given to any overseeing administrators so they may relay the information to investigative parties.

Vehicle Descriptions
====================
Vehicle descriptions via /ed should be used to present the physical features of the car, not internal specifications or information which cannot be readily seen from the outside.

CCTV Cameras
============
CCTV Cameras are by default, roleplayed as a 90 degree angle camera with 480p resolution at 5 frames per second. The data must be stored somewhere when roleplaying the install. All CCTV camera installations/upgrades must be approved by an administrator and added to the interior note. Footage is wiped at the end of the week if nothing of significance has occurred unless otherwise specified.

All government buildings and gas stations are assumed to have sufficient cameras to cover most common angles both inside and outside.

Roleplay Zone
=============
All roleplay must be done within a confined zone known as "`Los Santos County <https://imgkk.com/i/44da.jpg>`_". There are exceptions for dynamic situations such as car chases which may naturally lead outside of the roleplay zone. Additional exceptions may be specific ones approved by the Upper Administration Team such as the drag strip in Las Venturas.
